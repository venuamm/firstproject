package com.bestbuy.testing.constants;

public class DataConstants {
	public static final String URL="url";
    public static final String HOME_PAGE_TITLE="homepageTitle";
    public static final String SIGNIN_PAGE_TITLE="signinPageTitle";
    public static final String EMAIL="email";
    public static final String PASSWORD="password";
    public static final String SIGNIN_TEXT_VERIFY="signInText";
    public static final String GREETING_MSG_VERIFY="greetingMsgs";
    public static final String MEMBER_ID="memberId";
    public static final String REWARD_POINTS="rewardPoints";
    public static final String FIRST_NAME="firstName";
    public static final String LAST_NAME="lastName";
    public static final String ADDRESS="address";
    public static final String APARTMENT="apt";
    public static final String CITY="city";
    public static final String STATE="state";
    public static final String ZIP_CODE="zipCode";
    public static final String PHONE="phone";
    public static final String ADDRESS_SUCCESS_MSG_VERIFY="addressSuccessMsg";
    public static final String ADDRESS_VERIFY="savedAddress";
    public static final String ADDRESS_REMOVE_MSG_VERIFY="removeAddrSucessMsg";
    public static final String CREDITCARD_NUMBER="cardNumber";
    public static final String CARD_EXP_MONTH="expMnth";
    public static final String CARD_EXP_YEAR="expYear";
    public static final String CREDITCARD_SUCCESS_MSG="creditCardSuccessMsg";
    public static final String CARD_REMOVE_VERIFY="cardRemoveVerify";
    public static final String SIGNOUT_PAGE_VERIFY="signOutPageTitle";

}
