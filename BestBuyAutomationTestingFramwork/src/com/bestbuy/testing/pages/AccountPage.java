package com.bestbuy.testing.pages;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bestbuy.testing.constants.ElementConstants;
import com.bestbuy.testing.singleton.BestBuyDriver;
import com.bestbuy.testing.singleton.PropertyLoader;

public class AccountPage {
public static String greetintMesg(){
	Properties props=PropertyLoader.getProps();
	WebDriver driver=BestBuyDriver.getWebDriver();
	WebDriverWait wait=new WebDriverWait(driver,10);
	WebElement nameTag=wait.until(ExpectedConditions.elementToBeClickable(By.id(props.getProperty(ElementConstants.ACCOUNT))));
	nameTag.click();
	WebElement accountHome=driver.findElement(By.linkText(props.getProperty(ElementConstants.ACCOUNT_HOME_LINK)));
	accountHome.click();
	WebElement greetingMsg=driver.findElement(By.className(props.getProperty(ElementConstants.GREETING_MSG)));
	String greetingMg= greetingMsg.getText();
	String[] splitMsg=greetingMg.split("\\n",1);
	return splitMsg[0].replaceAll("\\n|\\s", "");
}

public static String memberId(){
	Properties props=PropertyLoader.getProps();
	WebDriver driver=BestBuyDriver.getWebDriver();
	WebElement member=driver.findElement(By.className(props.getProperty(ElementConstants.WELCOME_MEMBER_ID)));
	String id=member.getText();
	String[] splitId=id.split("\\s+");
	return splitId[splitId.length-1];
}

public static String rewardPoints(){
	Properties props=PropertyLoader.getProps();
	WebDriver driver=BestBuyDriver.getWebDriver();
	WebElement rewards=driver.findElement(By.className(props.getProperty(ElementConstants.REWARDS_NUMBER)));
	return rewards.getText();
}
}
