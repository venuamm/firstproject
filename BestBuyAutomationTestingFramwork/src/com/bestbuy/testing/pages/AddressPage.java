package com.bestbuy.testing.pages;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.constants.ElementConstants;
import com.bestbuy.testing.singleton.BestBuyDriver;
import com.bestbuy.testing.singleton.PropertyLoader;

public class AddressPage {
	
	
	public static String addAddress(){
		WebDriver driver=BestBuyDriver.getWebDriver();
		Properties props=PropertyLoader.getProps();
		WebElement addAddres=driver.findElement(By.linkText(props.getProperty(ElementConstants.ADD_SHIPPING_ADDRESS_BUTTON)));
		addAddres.click();
		WebElement fName=driver.findElement(By.id(props.getProperty(ElementConstants.FIRST_NAME_INPUT)));
		fName.sendKeys(props.getProperty(DataConstants.FIRST_NAME));
		WebElement lName=driver.findElement(By.id(props.getProperty(ElementConstants.LAST_NAME_INPUT)));
		lName.sendKeys(props.getProperty(DataConstants.LAST_NAME));
		WebElement address=driver.findElement(By.id(props.getProperty(ElementConstants.ADDRESS_INPUT)));
		address.sendKeys(props.getProperty(DataConstants.ADDRESS));
		WebElement apt=driver.findElement(By.id(props.getProperty(ElementConstants.APARTMENT_INPUT)));
		apt.sendKeys(props.getProperty(DataConstants.APARTMENT));
		WebElement city=driver.findElement(By.id(props.getProperty(ElementConstants.CITY_INPUT)));
		city.sendKeys(props.getProperty(DataConstants.CITY));
		WebElement state=driver.findElement(By.id(props.getProperty(ElementConstants.STATE_INPUT)));
		Select selState=new Select(state);
		selState.selectByValue(props.getProperty(DataConstants.STATE));
		WebElement zip=driver.findElement(By.id(props.getProperty(ElementConstants.ZIP_CODE_INPUT)));
		zip.sendKeys(props.getProperty(DataConstants.ZIP_CODE));
		WebElement phone=driver.findElement(By.id(props.getProperty(ElementConstants.PHONE_NUMBER_INPUT)));
		phone.sendKeys(props.getProperty(DataConstants.PHONE));
		WebElement submit=driver.findElement(By.className(props.getProperty(ElementConstants.ADDRESS_SUBMIT_BLOCK)));
		WebElement submitButton=submit.findElement(By.className(props.getProperty(ElementConstants.ADDRESS_SUBMIT_BUTTON)));
		submitButton.click();
		WebElement addressSuccessMsg=driver.findElement(By.className(props.getProperty(ElementConstants.SUCCESS_MSG_VERIFY)));
		return addressSuccessMsg.getText();
	}
  
	public static String confirmAddressPage(){
		WebDriver driver=BestBuyDriver.getWebDriver();
		Properties props=PropertyLoader.getProps();
		WebElement address=driver.findElement(By.tagName(props.getProperty(ElementConstants.ADDRESS_VERIFY)));
		String savedAddress=address.getText();
		String[] splitSavedAddress=savedAddress.split("\\n",1);
		return splitSavedAddress[0].replaceAll("\\n", ",");
	}
	
	public static String removeAddress(){
		String s="";
		WebDriver driver=BestBuyDriver.getWebDriver();
		Properties props=PropertyLoader.getProps();
		WebElement remove=driver.findElement(By.className(props.getProperty(ElementConstants.ADDRESS_REMOVE_BUTTON)));
		remove.click();
		WebElement address=driver.findElement(By.tagName(props.getProperty(ElementConstants.ADDRESS_VERIFY)));
		String savedAddress=address.getText();
		String[] splitSavedAddress=savedAddress.split("\\n",1);
		String returnAddress=splitSavedAddress[0].replaceAll("\\n", ",");
		String dataAddress=props.getProperty(DataConstants.ADDRESS_VERIFY);
		  if(dataAddress.equals(returnAddress)){
			WebElement yesRemove=driver.findElement(By.className(props.getProperty(ElementConstants.ADDRESS_REMOVE_CONFIRM_BLOCK)));
			WebElement removeButton=yesRemove.findElement(By.className(props.getProperty(ElementConstants.REMOVE_CONFIRM_BUTTON)));
			removeButton.click();
//			WebElement status=driver.findElement(By.className("profileAddressBookTitle"));
			WebElement alertClass=driver.findElement(By.className(props.getProperty(ElementConstants.REMOVE_MSG_VERIFY)));
		    s=alertClass.getText();
		  }
		  else{
			   s="Address not found"; 
		  }
return s;
	}
}
