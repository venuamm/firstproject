package com.bestbuy.testing.pages;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.constants.ElementConstants;
import com.bestbuy.testing.singleton.BestBuyDriver;
import com.bestbuy.testing.singleton.PropertyLoader;

public class CardPage {
public static String addCard(){
	WebDriver driver=BestBuyDriver.getWebDriver();
	Properties props=PropertyLoader.getProps();
	WebElement addButton=driver.findElement(By.className(props.getProperty(ElementConstants.ADD_CREDIT_CARD_BUTTON)));
	addButton.click();
	WebElement cardNumb=driver.findElement(By.id(props.getProperty(ElementConstants.CARD_NUMBER_INPUT)));
	cardNumb.sendKeys(props.getProperty(DataConstants.CREDITCARD_NUMBER));
	WebElement expMnth=driver.findElement(By.id(props.getProperty(ElementConstants.CARD_EXPIRY_MONTH_INPUT)));
	Select selMnth=new Select(expMnth);
	selMnth.selectByValue(props.getProperty(DataConstants.CARD_EXP_MONTH));
	WebElement expYear=driver.findElement(By.id(props.getProperty(ElementConstants.CARD_EXPIRY_YEAR_INPUT)));
	Select selYear =new Select(expYear);
	selYear.selectByValue(props.getProperty(DataConstants.CARD_EXP_YEAR));
	WebElement fName=driver.findElement(By.name(props.getProperty(ElementConstants.FIRST_NAME_INPUT)));
	fName.sendKeys(props.getProperty(DataConstants.FIRST_NAME));
	WebElement lName=driver.findElement(By.name(props.getProperty(ElementConstants.LAST_NAME_INPUT)));
	lName.sendKeys(props.getProperty(DataConstants.LAST_NAME));
	WebElement address=driver.findElement(By.name(props.getProperty(ElementConstants.ADDRESS_INPUT)));
	address.sendKeys(props.getProperty(DataConstants.ADDRESS));
	WebElement apt=driver.findElement(By.name(props.getProperty(ElementConstants.CITY_INPUT)));
	apt.sendKeys(props.getProperty(DataConstants.CITY));
	WebElement state=driver.findElement(By.name(props.getProperty(ElementConstants.STATE_INPUT)));
	Select selState=new Select(state);
	selState.selectByValue(props.getProperty(DataConstants.STATE));
	WebElement zip=driver.findElement(By.name(props.getProperty(ElementConstants.ZIP_CODE_INPUT)));
	zip.sendKeys(props.getProperty(DataConstants.ZIP_CODE));
	WebElement phn=driver.findElement(By.name(props.getProperty(ElementConstants.PHONE_NUMBER_INPUT)));
	phn.sendKeys(props.getProperty(DataConstants.PHONE));
	WebElement submit=driver.findElement(By.name(props.getProperty(ElementConstants.CARD_DETAILS_SUBMIT_BUTTON)));
	submit.click();
	WebElement successMsg=driver.findElement(By.className(props.getProperty(ElementConstants.SUCCESS_MSG_VERIFY)));
	return successMsg.getText();
}

public static String removeCard(){
	WebDriver driver=BestBuyDriver.getWebDriver();
	Properties props=PropertyLoader.getProps();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
	WebDriverWait wait= new WebDriverWait(driver,5);
	WebElement remove=wait.until(ExpectedConditions.elementToBeClickable(By.linkText(props.getProperty(ElementConstants.REMOVE_CARD_BUTTON))));
//	WebElement cardActions=driver.findElement(By.className("standardItem"));
// 	WebElement remove=cardActions.findElement(By.className("remove"));
	remove.click();
	WebElement yesRemove=driver.findElement(By.className(props.getProperty(ElementConstants.CARD_REMOVE_CONFIRM_BUTTON)));
	yesRemove.click();
	WebElement successMsg=driver.findElement(By.className(props.getProperty(ElementConstants.REMOVE_MSG_VERIFY)));
	return successMsg.getText();
}
}
