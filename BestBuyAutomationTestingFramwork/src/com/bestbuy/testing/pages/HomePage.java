package com.bestbuy.testing.pages;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.constants.ElementConstants;
import com.bestbuy.testing.singleton.BestBuyDriver;
import com.bestbuy.testing.singleton.PropertyLoader;

public class HomePage {
public static String loadPage(){
	Properties props=PropertyLoader.getProps();
	WebDriver driver=BestBuyDriver.getWebDriver();
	driver.get(props.getProperty(DataConstants.URL));
	return driver.getTitle();
}
public static void closePopUp(){
	Properties props=PropertyLoader.getProps();
	WebDriver driver=BestBuyDriver.getWebDriver();
	try{
		driver.findElement(By.className(props.getProperty(ElementConstants.CLOSE_POPUP))).click();
	} catch (Exception e) {
		e.printStackTrace();
	}
}
}
