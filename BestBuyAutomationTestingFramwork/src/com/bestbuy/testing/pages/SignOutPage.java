package com.bestbuy.testing.pages;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bestbuy.testing.constants.ElementConstants;
import com.bestbuy.testing.singleton.BestBuyDriver;
import com.bestbuy.testing.singleton.PropertyLoader;

public class SignOutPage {
	
	public static String signOut(){
	WebDriver driver=BestBuyDriver.getWebDriver();
	Properties props=PropertyLoader.getProps();
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
	WebDriverWait wait=new WebDriverWait(driver,10);
	WebElement account=wait.until(ExpectedConditions.elementToBeClickable(By.id(props.getProperty(ElementConstants.ACCOUNT))));
	account.click();
	WebElement signout=driver.findElement(By.id(props.getProperty(ElementConstants.SIGN_OUT_BUTTON)));
	signout.click();
	return driver.getTitle();
	}	
}
