package com.bestbuy.testing.pages;

import java.util.Properties;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.constants.ElementConstants;
import com.bestbuy.testing.singleton.BestBuyDriver;
import com.bestbuy.testing.singleton.PropertyLoader;

public class SigninPage {
	public static String signinPag(){
   Properties props=PropertyLoader.getProps();
   WebDriver driver=BestBuyDriver.getWebDriver();
   WebElement acc=driver.findElement(By.id(props.getProperty(ElementConstants.ACCOUNT)));
   acc.click();
   driver.findElement(By.className(props.getProperty(ElementConstants.SIGN_IN))).click();
   return driver.getTitle();
	}
	
	public static String signin(){
		WebDriver driver=BestBuyDriver.getWebDriver();
		Properties props=PropertyLoader.getProps();
		WebDriverWait wait=new WebDriverWait(driver, 10);
		driver.findElement(By.id(props.getProperty(ElementConstants.EMAIL))).sendKeys(props.getProperty(DataConstants.EMAIL));
		driver.findElement(By.id(props.getProperty(ElementConstants.PASSWORD))).sendKeys(props.getProperty(DataConstants.PASSWORD));
		driver.findElement(By.className(props.getProperty(ElementConstants.SIGN_IN_BUTTON))).click();
		try{
//			WebElement noThanks=driver.findElement(By.id("survey_invite_no"));
//			noThanks.click();
			Alert ale=driver.switchTo().alert();
			ale.dismiss();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		WebElement nameTag=wait.until(ExpectedConditions.elementToBeClickable(By.id(props.getProperty(ElementConstants.ACCOUNT))));
		return nameTag.getText();
	}
}
