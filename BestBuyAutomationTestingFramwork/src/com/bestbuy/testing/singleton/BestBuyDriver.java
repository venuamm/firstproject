package com.bestbuy.testing.singleton;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class BestBuyDriver {
	private static WebDriver driver;
	
	private BestBuyDriver(){
	}
	
	public static WebDriver getWebDriver(){
		if(driver==null){
			ChromeOptions ob=new ChromeOptions();
			Map<String,Object> maps=new HashMap();
			maps.put("credentials_enable_service", false);
			maps.put("profile.password_manager_enabled", false);
			ob.setExperimentalOption("prefs", maps);
			System.setProperty("webdriver.chrome.driver",  "/Users/venuamm/Documents/workspace/Drivers/chromedriver2");
			driver=new ChromeDriver(ob);
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			driver.manage().window().maximize();
		}
		return driver;
	}

}
