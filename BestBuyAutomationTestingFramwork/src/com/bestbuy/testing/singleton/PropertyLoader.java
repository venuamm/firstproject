package com.bestbuy.testing.singleton;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {
     private static Properties props;
     
     private PropertyLoader(){
     }
     public static Properties getProps(){ 
    	 if(props==null){
    		 props=new Properties(); 
    		 FileInputStream is;
			try {
				is = new FileInputStream("data.properties");
				props.load(is);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
    		 FileInputStream is1;
			try {
				is1 = new FileInputStream("element.properties");
				 props.load(is1);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}	
    	 }
    	 return props;
     }
}
