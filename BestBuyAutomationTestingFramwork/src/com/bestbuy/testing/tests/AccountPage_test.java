package com.bestbuy.testing.tests;



import org.junit.Assert;
import org.testng.annotations.Test;

import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.pages.AccountPage;
import com.bestbuy.testing.singleton.PropertyLoader;

public class AccountPage_test {
@Test
public void accountPage_test(){
	  String greetingMsg=AccountPage.greetintMesg();
	  String dataMsg=PropertyLoader.getProps().getProperty(DataConstants.GREETING_MSG_VERIFY);
	  Assert.assertEquals(dataMsg, greetingMsg);
      
	  String memberId=AccountPage.memberId();
	  Assert.assertEquals(memberId, PropertyLoader.getProps().getProperty(DataConstants.MEMBER_ID));
	  String rewards=AccountPage.rewardPoints();
	  Assert.assertEquals(rewards, PropertyLoader.getProps().getProperty(DataConstants.REWARD_POINTS));
	  
}
}
