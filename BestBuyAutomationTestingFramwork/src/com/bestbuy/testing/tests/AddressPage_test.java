package com.bestbuy.testing.tests;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.pages.AddressPage;
import com.bestbuy.testing.singleton.PropertyLoader;

public class AddressPage_test {
  @Test
  public void addAddress_test() throws InterruptedException {
	  String successMsg=AddressPage.addAddress();
	  Assert.assertEquals(successMsg, PropertyLoader.getProps().getProperty(DataConstants.ADDRESS_SUCCESS_MSG_VERIFY));
	  String savedAddress=AddressPage.confirmAddressPage();
      String dataAddress=PropertyLoader.getProps().getProperty(DataConstants.ADDRESS_VERIFY);
      Assert.assertEquals(dataAddress, savedAddress);
	  try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
	String s=AddressPage.removeAddress();
	Thread.sleep(5000);
//    Assert.assertEquals(PropertyLoader.getProps().getProperty(DataConstants.AddressRemoveMsg),s);
  }
}

