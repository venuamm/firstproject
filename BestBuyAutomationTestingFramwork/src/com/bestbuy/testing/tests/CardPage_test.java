package com.bestbuy.testing.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.pages.AccountPage;
import com.bestbuy.testing.pages.CardPage;
import com.bestbuy.testing.singleton.PropertyLoader;

public class CardPage_test {
  @Test
  public void  addCard_test(){
	  AccountPage.greetintMesg();
	  String s=CardPage.addCard();
	  Assert.assertEquals(PropertyLoader.getProps().getProperty(DataConstants.CREDITCARD_SUCCESS_MSG), s);
	  String n=CardPage.removeCard();
	  Assert.assertEquals(n, PropertyLoader.getProps().getProperty(DataConstants.CARD_REMOVE_VERIFY));
  }
}
