package com.bestbuy.testing.tests;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.bestbuy.testing.constants.DataConstants;
import com.bestbuy.testing.pages.SignOutPage;
import com.bestbuy.testing.singleton.BestBuyDriver;
import com.bestbuy.testing.singleton.PropertyLoader;


public class SignOutPage_test {
  @Test
  public void SignOut_test() {
	  String s=SignOutPage.signOut();
	  Assert.assertEquals(PropertyLoader.getProps().getProperty(DataConstants.SIGNOUT_PAGE_VERIFY), s);
  }
  @AfterTest
  public void browserQuit(){
	  try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
	  BestBuyDriver.getWebDriver().quit();
  }
}
