package com.bestbuy.testing.tests;

import java.util.Arrays;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.bestbuy.testing.constants.DataConstants;

import com.bestbuy.testing.pages.HomePage;
import com.bestbuy.testing.pages.SigninPage;
import com.bestbuy.testing.singleton.PropertyLoader;

public class SignonPage_Test {
  @Test
  public void homepage_test(){
	  String h=HomePage.loadPage();
	  String s=PropertyLoader.getProps().getProperty(DataConstants.HOME_PAGE_TITLE);
	  Assert.assertEquals(s, h);
	  HomePage.closePopUp();
	  String sign=SigninPage.signinPag();
	  Assert.assertEquals(sign, PropertyLoader.getProps().getProperty(DataConstants.SIGNIN_PAGE_TITLE));
	  String n=SigninPage.signin();
	  Assert.assertEquals(n, PropertyLoader.getProps().getProperty(DataConstants.SIGNIN_TEXT_VERIFY));

  }
}
